package com.micro.payment.app.service.impl;

import com.micro.payment.app.entity.Payment;
import com.micro.payment.app.exception.NotFoundException;
import com.micro.payment.app.repository.PaymentRepository;
import com.micro.payment.app.request.CreatePaymentRequest;
import com.micro.payment.app.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
public class PaymentServiceImpl implements PaymentService {

    @Autowired PaymentRepository paymentRepository;

    @Override
    public Payment createPayment(CreatePaymentRequest createPaymentRequest) {
        Payment payment = new Payment();
        payment.setInvoice(UUID.randomUUID().toString());
        payment.setOrderId(createPaymentRequest.getOrderId());
        payment.setAmount(createPaymentRequest.getAmount());
        payment.setCreatedAt(new Date());
        payment.setUpdatedAt(new Date());

        paymentRepository.save(payment);

        return payment;
    }

    @Override
    public Payment getPaymentById(String id) {
        Payment payment = findPaymentByIdOrThrowNotFound(id);

        return payment;
    }

    @Override
    public List<Payment> getAllPayments() {
        return paymentRepository.findAll();
    }

    private Payment findPaymentByIdOrThrowNotFound(String id) {
        Payment payment = paymentRepository.findById(id).orElse(null);

        if (payment == null){
            throw new NotFoundException();
        } else {
            return payment;
        }
    }

    @Override
    public Payment findPaymentByOrderId(String id) {
        Payment payment = paymentRepository.findByOrderId(id);

        return payment;
    }

    @Override
    public boolean deleteAllPayment() {
        paymentRepository.deleteAll();

        return true;
    }
}
