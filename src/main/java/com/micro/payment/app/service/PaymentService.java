package com.micro.payment.app.service;

import com.micro.payment.app.entity.Payment;
import com.micro.payment.app.request.CreatePaymentRequest;

import java.util.List;

public interface PaymentService {

    public Payment createPayment(CreatePaymentRequest createPaymentRequest);

    public Payment getPaymentById(String id);

    public List<Payment> getAllPayments();

    public Payment findPaymentByOrderId(String id);

    public boolean deleteAllPayment();
}
